﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using OntologyAppDBConnector;
using Ontology_Module;
using Security_Module;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace Change_Module
{
    public partial class frmChangeModule : Form
    {
        private frmAuthenticate objFrmAuthenticate;
        private SplashScreen_OntologyModule SplashScreen;
        private AboutBox_OntologyItem AboutBox;
        private clsLocalConfig objLocalConfig;
        private UserControl_TicketTree objUserControl_TicketTree;
        private UserControl_TicketList objUserContorl_TicketList;
        private clsDataWork_Ticket objDataWork_Ticket;
        private clsArgumentParsing argumentParsing;
        private clsOntologyItem oItemTicket;

        public frmChangeModule()
        {
            InitializeComponent();
            
            Application.DoEvents();
            SplashScreen = new SplashScreen_OntologyModule();
            SplashScreen.Show();
            SplashScreen.Refresh();

            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            ParseArguments();

            initialize();
        }

        private void ParseArguments()
        {
            oItemTicket = null;
            argumentParsing = new clsArgumentParsing(objLocalConfig.Globals,Environment.GetCommandLineArgs().ToList());

            if (argumentParsing.OList_Items != null && argumentParsing.OList_Items.Count == 1)
            {
                if (argumentParsing.OList_Items.First().GUID_Parent == objLocalConfig.OItem_Type_Process_Ticket.GUID)
                {
                    oItemTicket = argumentParsing.OList_Items.First();

                }
                

            }
        }

        private void set_DBConnection()
        {

        }

        private void initialize()
        {


            objDataWork_Ticket = new clsDataWork_Ticket(objLocalConfig);
            var objOItem_Result = objDataWork_Ticket.GetData_Tickets(oItemTicket != null ? oItemTicket.GUID : null);
            
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                MessageBox.Show("Die Ticketliste konnte nicht geladen werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                
            }
            else
            {
                objOItem_Result = objDataWork_Ticket.GetData_TicketListTree();

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                {
                    MessageBox.Show("Die Ticketliste konnte nicht geladen werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
            }


        }

        private void timer_TicketList_Tick(object sender, EventArgs e)
        {
            
            if (objDataWork_Ticket.OItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                toolStripProgressBar_TicketList.Value = 0;
                timer_TicketList.Stop();
                var objOItem_Result = objDataWork_Ticket.FillTicketTable();
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                {
                    MessageBox.Show("Die Ticketliste kann nicht ermittelt werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }
                else
                {
                    objUserContorl_TicketList.initialize();
                    
                    if (oItemTicket == null)
                    {
                        objUserContorl_TicketList.ShowAll = toolStripButton_OpenAll.Checked;        
                    }
                    else
                    {
                        
                        objUserContorl_TicketList.initialize();
                        objUserContorl_TicketList.ShowAll = true;
                        var ticketOpened = objUserContorl_TicketList.OpenTicketByTicketItem(oItemTicket);
                        if (!ticketOpened)
                        {
                            MessageBox.Show(this, "Das Ticket konnte nicht geöffnet werden!",
                                "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    
                }
                
            }
            else if (objDataWork_Ticket.OItem_Result.GUID == objLocalConfig.Globals.LState_Nothing.GUID)
            {
                toolStripProgressBar_TicketList.Value = 50;
            }
            else
            {
                timer_TicketList.Stop();
                MessageBox.Show("Die Ticketliste kann nicht ermittelt werden!", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
                toolStripProgressBar_TicketList.Value = 0;
                
            }
        }

        private void frmChangeModule_Load(object sender, EventArgs e)
        {
            if (SplashScreen != null)
            {
                SplashScreen.Close();
            }

            if (InitializeForm().GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                timer_TicketList.Start();
                
            }
            else
            {
                Close();
            }
        }

        private clsOntologyItem InitializeForm()
        {
            objFrmAuthenticate = new frmAuthenticate(objLocalConfig.Globals, true, true, frmAuthenticate.ERelateMode.User_To_Group);
            objFrmAuthenticate.ShowDialog(this);
            if (objFrmAuthenticate.DialogResult == DialogResult.OK)
            {
                objLocalConfig.OItem_User = objFrmAuthenticate.OItem_User;
                objLocalConfig.OItem_Group = objFrmAuthenticate.OItem_Group;

                objUserControl_TicketTree = new UserControl_TicketTree(objLocalConfig, objDataWork_Ticket, !toolStripButton_OpenAll.Checked);
                objUserControl_TicketTree.Dock = DockStyle.Fill;

                objUserControl_TicketTree.SelTicketList += SelectedTicketList;
                objUserControl_TicketTree.Relate += objUserControl_TicketTree_Relate;

                splitContainer1.Panel1.Controls.Add(objUserControl_TicketTree);
                objUserContorl_TicketList = new UserControl_TicketList(objLocalConfig, objDataWork_Ticket, null);
                objUserContorl_TicketList.Dock = DockStyle.Fill;
                splitContainer1.Panel2.Controls.Add(objUserContorl_TicketList);

                return objLocalConfig.Globals.LState_Success.Clone();
            }
            else
            {
                return objLocalConfig.Globals.LState_Error.Clone();
            }            
        }

        void objUserControl_TicketTree_Relate(clsOntologyItem OItem_TicketList)
        {
            objUserContorl_TicketList.OItem_TicketList_Relate = OItem_TicketList;
        }

        private void SelectedTicketList(clsOntologyItem OItem_TicketList)
        {
            objUserContorl_TicketList.SetTicketList(OItem_TicketList);
        }

        private void toolStripButton_OpenAll_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton_OpenAll_CheckStateChanged(object sender, EventArgs e)
        {
            objUserContorl_TicketList.ShowAll = toolStripButton_OpenAll.Checked;
            if (toolStripButton_OpenAll.Checked)
            {
                toolStripButton_OpenAll.Text = "x_All";
            }
            else
            {
                toolStripButton_OpenAll.Text = "x_Open";
            }
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox = new AboutBox_OntologyItem();
            AboutBox.ShowDialog(this);
        }

    }
}
