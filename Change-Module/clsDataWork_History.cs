﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ontology_Module;
using System.Threading;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace Change_Module
{
    class clsDataWork_History
    {

        

        private clsLocalConfig objLocalConfig;
        private clsOntologyItem objOItem_TicketProcessLog;
        public clsOntologyItem objOItem_Result_LogEntries { get; set; }
        public clsOntologyItem objOItem_Result_DateTimeStamps { get; set; }
        public clsOntologyItem objOItem_Result_Messages { get; set; }
        public clsOntologyItem objOItem_Result_LogStates { get; set; }
        private DataSet_ChangeModule.chngview_LogentriesOfProcessLogDataTable chngviewT_LogentriesOfProcessLog = new DataSet_ChangeModule.chngview_LogentriesOfProcessLogDataTable();

        private OntologyModDBConnector objDBLevel_History_LogEntries;
        private OntologyModDBConnector objDBLevel_History_DateTimeStamps;
        private OntologyModDBConnector objDBLevel_History_Messages;
        private OntologyModDBConnector objDBLevel_History_LogStates;
        private OntologyModDBConnector objDBLevel_ProcessTickets_To_LogEntries;

        private Thread objThread_LogEntries;
        private Thread objThread_DateTimeStamps;
        private Thread objThread_Messages;
        private Thread objThread_LogStates;

        

        public DataSet_ChangeModule.chngview_LogentriesOfProcessLogDataTable ChngView_LogEntriesOfProcessLog
        {
            get { return chngviewT_LogentriesOfProcessLog; }
        }

        public clsDataWork_History(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            set_DBConnection();
        }


        private void set_DBConnection()
        {
            objDBLevel_History_LogEntries = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_History_DateTimeStamps = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_History_Messages = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_History_LogStates = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_ProcessTickets_To_LogEntries = new OntologyModDBConnector(objLocalConfig.Globals);
        }

        public clsOntologyItem GetData(clsOntologyItem OItem_Selected)
        {
            clsOntologyItem objOItem_Result;

            objOItem_TicketProcessLog = OItem_Selected;

            objOItem_Result_LogEntries = objLocalConfig.Globals.LState_Nothing;
            objOItem_Result_LogStates = objLocalConfig.Globals.LState_Nothing;
            objOItem_Result_DateTimeStamps = objLocalConfig.Globals.LState_Nothing;
            objOItem_Result_Messages = objLocalConfig.Globals.LState_Nothing;

            try
            {
                objThread_LogEntries.Abort();
            }
            catch (Exception e)
            {
            }

            //try
            //{
            //    objThread_LogStates.Abort();
            //}
            //catch (Exception e)
            //{
            //}

            //try
            //{
            //    objThread_DateTimeStamps.Abort();
            //}
            //catch (Exception e)
            //{
            //}

            //try
            //{
            //    objThread_Messages.Abort();
            //}
            //catch (Exception e)
            //{
            //}

            objThread_LogEntries = new Thread(GetData_LogEntriesOfProcessLog);
            //objThread_DateTimeStamps = new Thread(GetData_DateTimeStampsOfLogEntries);
            //objThread_Messages = new Thread(GetData_MessagesOfLogEntries);
            //objThread_LogStates = new Thread(GetData_LogStatesOfLogEntries);

            objThread_LogEntries.Start();
            //objThread_DateTimeStamps.Start();
            //objThread_Messages.Start();
            //objThread_LogStates.Start();
            
            objOItem_Result = objLocalConfig.Globals.LState_Success;

            return objOItem_Result;
        }

        public clsOntologyItem HasFinished()
        {
            clsOntologyItem objOItem_Result;

            if (objOItem_Result_LogEntries.GUID == objLocalConfig.Globals.LState_Success.GUID
                && objOItem_Result_DateTimeStamps.GUID == objLocalConfig.Globals.LState_Success.GUID
                && objOItem_Result_Messages.GUID == objLocalConfig.Globals.LState_Success.GUID
                && objOItem_Result_LogStates.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                FillHistoryTable();
                objOItem_Result = objLocalConfig.Globals.LState_Success;
            }
            else if (objOItem_Result_LogEntries.GUID == objLocalConfig.Globals.LState_Error.GUID
                     || objOItem_Result_DateTimeStamps.GUID == objLocalConfig.Globals.LState_Error.GUID
                     || objOItem_Result_Messages.GUID == objLocalConfig.Globals.LState_Error.GUID
                     || objOItem_Result_LogStates.GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                objOItem_Result = objLocalConfig.Globals.LState_Error;
            }
            else
            {
                objOItem_Result = objLocalConfig.Globals.LState_Nothing;
            }

            return objOItem_Result;
        }

        private clsOntologyItem FillHistoryTable()
        {
            clsOntologyItem objOItem_Result = objLocalConfig.Globals.LState_Success;

            chngviewT_LogentriesOfProcessLog.Clear();

            var objList_LogEntries = from objLogEntry in objDBLevel_History_LogEntries.ObjectRels
                                     join objDateTimeStamp in objDBLevel_History_DateTimeStamps.ObjAtts on objLogEntry.ID_Other equals objDateTimeStamp.ID_Object
                                     join objMessage in objDBLevel_History_Messages.ObjAtts on objLogEntry.ID_Other equals objMessage.ID_Object into objMessages
                                     from objMessage in objMessages.DefaultIfEmpty()
                                     join objLogState in objDBLevel_History_LogStates.ObjectRels on objLogEntry.ID_Other equals objLogState.ID_Object
                                     select new { objLogEntry, objDateTimeStamp, objMessage, objLogState };

            foreach (var objLogentry in objList_LogEntries)
            {
                chngviewT_LogentriesOfProcessLog.Rows.Add(objLogentry.objLogEntry.ID_Other,
                                                          objLogentry.objLogEntry.Name_Other,
                                                          objLogentry.objDateTimeStamp.ID_Attribute,
                                                          objLogentry.objDateTimeStamp.Val_Date,
                                                          objLogentry.objLogState.ID_Other,
                                                          objLogentry.objLogState.Name_Other,
                                                          objLogentry.objMessage.ID_Attribute,
                                                          objLogentry.objMessage.Val_String);
            }

            return objOItem_Result;
        }

        private void GetData_LogEntriesOfProcessLog()
        {
            List<clsObjectRel> objOList_LogEntries = new List<clsObjectRel>();

            objOItem_Result_LogEntries = objLocalConfig.Globals.LState_Nothing;

            objOList_LogEntries.Add(new clsObjectRel
            {
                ID_Object = objOItem_TicketProcessLog.GUID,
                ID_Parent_Other = objLocalConfig.OItem_Type_LogEntry.GUID,
                ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Done.GUID,
                Ontology = objLocalConfig.Globals.Type_Object
            });

            objOItem_Result_LogEntries = objDBLevel_History_LogEntries.GetDataObjectRel(objOList_LogEntries,
                                                                    doIds: false);

            if (objOItem_Result_LogEntries.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                GetData_DateTimeStampsOfLogEntries();
                if (objOItem_Result_DateTimeStamps.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    GetData_MessagesOfLogEntries();
                    if (objOItem_Result_DateTimeStamps.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        GetData_LogStatesOfLogEntries();
                    }
                }
            }
         
        }

        public clsOntologyItem GetProcessLogOfLogentry(clsOntologyItem logEntry)
        {
            var searchProcessLog = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = logEntry.GUID,
                    ID_RelationType = objLocalConfig.OItem_RelationType_belonging_Done.GUID,
                    ID_Parent_Object = objLocalConfig.OItem_Type_Process_Log.GUID
                }
            };

            var result = objDBLevel_ProcessTickets_To_LogEntries.GetDataObjectRel(searchProcessLog);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var processLog = objDBLevel_ProcessTickets_To_LogEntries.ObjectRels.Select(procLoc => new clsOntologyItem
                {
                    GUID = procLoc.ID_Object,
                    Name = procLoc.Name_Object,
                    GUID_Parent = procLoc.ID_Parent_Object,
                    Type = objLocalConfig.Globals.Type_Object
                }).FirstOrDefault();

                result.OList_Rel = new List<clsOntologyItem>();
                if (processLog != null)
                {
                    result.OList_Rel.Add(processLog);
                }
            }

            return result;
        }

        private void GetData_DateTimeStampsOfLogEntries()
        {
            List<clsObjectAtt> objOList_DateTimeStamp = new List<clsObjectAtt>();
            objOItem_Result_DateTimeStamps = objLocalConfig.Globals.LState_Nothing;

            foreach (var objOItem_LogEntries in objDBLevel_History_LogEntries.ObjectRels)
            {
                objOList_DateTimeStamp.Add(new clsObjectAtt(null,
                                                        objOItem_LogEntries.ID_Other,
                                                        null,
                                                        objLocalConfig.OItem_Attribute_DateTimeStamp.GUID,
                                                        null));
            }

            if (objOList_DateTimeStamp.Any())
            {
                objOItem_Result_DateTimeStamps = objDBLevel_History_DateTimeStamps.GetDataObjectAtt(objOList_DateTimeStamp,
                                                                                   doIds: false);
            }
            else
            {
                objOItem_Result_DateTimeStamps = objLocalConfig.Globals.LState_Success;
            }
            
        }

        private void GetData_MessagesOfLogEntries()
        {
            List<clsObjectAtt> objOList_Message = new List<clsObjectAtt>();

            objOItem_Result_Messages = objLocalConfig.Globals.LState_Nothing;

            foreach (var objOItem_LogEntries in objDBLevel_History_LogEntries.ObjectRels)
            {
                objOList_Message.Add(new clsObjectAtt(null,
                                                  objOItem_LogEntries.ID_Other,
                                                  null,
                                                  objLocalConfig.OItem_Attribute_Message.GUID,
                                                  null));
            }


            if (objOList_Message.Any())
            {
                objOItem_Result_Messages = objDBLevel_History_Messages.GetDataObjectAtt(objOList_Message,
                                                                                      doIds: false);
            }
            else
            {
                objOItem_Result_Messages = objLocalConfig.Globals.LState_Success;
            }
        }

        private void GetData_LogStatesOfLogEntries()
        {
            List<clsObjectRel> objOList_LogStates = new List<clsObjectRel>();

            objOItem_Result_LogStates = objLocalConfig.Globals.LState_Nothing;

            foreach (var OItem_LogEntry in objDBLevel_History_LogEntries.ObjectRels)
            {
                objOList_LogStates.Add(new clsObjectRel
                {
                    ID_Object = OItem_LogEntry.ID_Other,
                    ID_Parent_Other = objLocalConfig.OItem_type_Logstate.GUID,
                    ID_RelationType = objLocalConfig.OItem_RelationType_provides.GUID,
                    Ontology = objLocalConfig.Globals.Type_Object
                });
            }



            if (objOList_LogStates.Any())
            {
                objOItem_Result_LogStates = objDBLevel_History_LogStates.GetDataObjectRel(objOList_LogStates,
                                                                                        doIds: false);
            }
            else
            {
                objOItem_Result_LogStates = objLocalConfig.Globals.LState_Success;
            }
            
        }

    }
}
