﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using Priority_Tagging_Module;

namespace Change_Module
{
    public partial class UserControl_History : UserControl
    {
        public delegate void ChoosenProcessLog(clsOntologyItem processLog);
        public event ChoosenProcessLog choosenProcessLog;

        private clsLocalConfig objLocalConfig;
        private clsOntologyItem objOItem_Selected;
        private clsDataWork_History objDataWork_History;

        private frmMain frmMainItem;

        public UserControl_History(clsLocalConfig LocalConfig )
        {
            objLocalConfig = LocalConfig;

            InitializeComponent();

            toggleEnable(false);

        }

        public void initialize(clsOntologyItem OItem_Selected)
        {

            objDataWork_History = new clsDataWork_History(objLocalConfig);

            objOItem_Selected = OItem_Selected;

            refreshItems();
            
        }

        public void refreshItems()
        {
            clsOntologyItem objOItem_Result;
            toolStripProgressBar_History.Value = 0;
            timer_History.Stop();

            toggleEnable(false);

            objOItem_Result = objDataWork_History.GetData(objOItem_Selected);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                timer_History.Start();
            }
            else
            {
                MessageBox.Show("Die Historie kann nicht ermittelt werden!", "Historie", MessageBoxButtons.OK);
            }
        }

        private void timer_History_Tick(object sender, EventArgs e)
        {
            if (objDataWork_History.HasFinished().GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                bindingSource_History.DataSource = objDataWork_History.ChngView_LogEntriesOfProcessLog;
                dataGridView_History.DataSource = bindingSource_History;
                toggleEnable(true);
                toolStripProgressBar_History.Value = 0;
                timer_History.Stop();

                dataGridView_History.Columns[0].Visible = false;
                dataGridView_History.Columns[1].Visible = false;
                dataGridView_History.Columns[2].Visible = false;
                dataGridView_History.Columns[4].Visible = false;
                dataGridView_History.Columns[6].Visible = false;
                bindingSource_History.Sort = "DateTimeStamp";
            }
            else if (objDataWork_History.HasFinished().GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                MessageBox.Show("Die Historie kann nicht ermittelt werden!", "Historie", MessageBoxButtons.OK);
                toolStripProgressBar_History.Value = 0;
            }
            else
            {
                toolStripProgressBar_History.Value = 50;
            }

        }

        private void toggleEnable(Boolean boolEnable)
        {
            
            dataGridView_History.Enabled = boolEnable;
            bindingNavigator_History.Enabled = boolEnable;
            bindingSource_History.Sort="";
            if (!boolEnable)
            {
                
                bindingSource_History.DataSource = null;
                dataGridView_History.DataSource = null;
            }
        }

        private void contextMenuStrip_History_Opening(object sender, CancelEventArgs e)
        {
            setPriorityToolStripMenuItem.Enabled = false;
            if (dataGridView_History.SelectedRows.Count > 0)
            {
                setPriorityToolStripMenuItem.Enabled = true;
            }
        }

        private void setPriorityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var rowItems = dataGridView_History.SelectedRows.Cast<DataGridViewRow>().ToList();



            frmMainItem = new frmMain(objLocalConfig.Globals, objLocalConfig.PriorityTaggingManager.LocalConfig.Globals.Type_Class, objLocalConfig.PriorityTaggingManager.LocalConfig.OItem_class_priority);
            frmMainItem.ShowDialog(this);

            if (frmMainItem.DialogResult == DialogResult.OK)
            {
                if (frmMainItem.Type_Applied != objLocalConfig.Globals.Type_Object)
                {
                    MessageBox.Show(this, "Bitte wählen Sie nur eine Priorität aus!", "Falsche Auswahl", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (frmMainItem.OList_Simple.Count != 1)
                {
                    MessageBox.Show(this, "Bitte wählen Sie nur eine Priorität aus!", "Falsche Auswahl", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                var selectedPriority = frmMainItem.OList_Simple.First();

                if (selectedPriority.GUID_Parent != objLocalConfig.PriorityTaggingManager.LocalConfig.OItem_class_priority.GUID)
                {
                    MessageBox.Show(this, "Bitte wählen Sie nur eine Priorität aus!", "Falsche Auswahl", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                var priority = objLocalConfig.PriorityTaggingManager.PriorityList.FirstOrDefault(prio => prio.IdPriorityItem == selectedPriority.GUID);

                if (priority == null)
                {
                    MessageBox.Show(this, "Die Priorität, die Sie ausgewählt haben, wurde noch nicht in die allgemeine Prioritätenliste aufgenommen!", "Prio kein standard", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                foreach (DataGridViewRow dgvSelected in rowItems)
                {
                    DataRowView drvSelected = (DataRowView) dgvSelected.DataBoundItem;
                    var logentry = new clsOntologyItem
                    {
                        GUID = drvSelected["GUID_LogEntry"].ToString(),
                        Name = drvSelected["Name_LogEntry"].ToString(),
                        GUID_Parent = objLocalConfig.OItem_Type_LogEntry.GUID,
                        Type = objLocalConfig.Globals.Type_Object
                    };


                    

                    var result = objLocalConfig.PriorityTaggingManager.SetPriorityTag(logentry, priority);
                    if (result.GUID == objLocalConfig.Globals.LState_Error.GUID)
                    {
                        MessageBox.Show(this, "Der Logeinträge konnten nicht mit der Priorität getagged werden!", "Tagging-Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                }
                
            }
        }

        private void dataGridView_History_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var idLogEntry = dataGridView_History.Rows[e.RowIndex].Cells[0].Value.ToString();
            var priority = objLocalConfig.PriorityReferences.FirstOrDefault(prio => prio.IdRef == idLogEntry);

            if (priority != null)
            {
                dataGridView_History.Rows[e.RowIndex].Cells[3].ToolTipText = priority.PriorityItem.NamePriorityItem;
                if (priority.PriorityItem.ForeColorItem != null)
                {
                    dataGridView_History.Rows[e.RowIndex].DefaultCellStyle.ForeColor = priority.PriorityItem.ForeColorItem.Color;
                    
                }

                if (priority.PriorityItem.BackColorItem != null)
                {
                    dataGridView_History.Rows[e.RowIndex].DefaultCellStyle.BackColor = priority.PriorityItem.BackColorItem.Color;

                }

            }

        }

        private void dataGridView_History_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var drvSelected = (DataRowView) dataGridView_History.Rows[e.RowIndex].DataBoundItem;

            var logEntry = new clsOntologyItem
            {
                GUID = drvSelected["GUID_LogEntry"].ToString(),
                Name = drvSelected["Name_LogEntry"].ToString(),
                GUID_Parent = objLocalConfig.OItem_Type_LogEntry.GUID,
                Type = objLocalConfig.Globals.Type_Object
            };

            var processLog = objDataWork_History.GetProcessLogOfLogentry(logEntry);

            if (processLog.GUID == objLocalConfig.Globals.LState_Error.GUID)
            {
                MessageBox.Show(this, "Der Prozessknoten konnte nicht ermittelt werden!", "Knoten nicht gefunden", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (processLog.OList_Rel.Any()  && choosenProcessLog != null)
            {
                choosenProcessLog(processLog.OList_Rel.First());
            }
        }
    }
}
